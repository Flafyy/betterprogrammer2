package com.active;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class BetterProgrammerTask2 {

    public static void sortIgnoringSpaces(String[] a) {
        /*
          Please implement this method to
          sort a given array of Strngs in alphabetical order
          ignoring spaces (' ' symbols) within the strings.
         */
        Arrays.sort(a, new SpaceIgnoreComparator());
    }

    public static void main(String[] args) {
        String [] dd = {"fff agg", "fff fff", "dadsfds", "dsds sds"};
        sortIgnoringSpaces(dd);
        System.out.println();
    }
}


class SpaceIgnoreComparator implements Comparator<String> {
    @Override
    public int compare(String a, String b) {
        return a.replace(" ", "").compareTo(b.replace(" ", ""));
    }
}