package com.active;

public class BetterProgrammerTask {

    public static Object[] reverseArray(Object[] a) {
        int length = a.length;
        Object[] newArr = new Object[length];
        length--;
        for (int i = 0; i < newArr.length; i++) {
            newArr[i] = a[length - i];
        }
        return newArr;
    }

    public static void main(String[] args) {
        Integer[] n = {1, 2, 3, 4, 5};
        Object[] l = reverseArray(n);
        System.out.println();
    }
}
