package com.active;

import java.util.List;

public class BetterProgrammerTask4 {

    static int[] monets = {50, 25, 10, 5, 1};

    public static int countWaysToProduceGivenAmountOfMoney(int cents) {
        /*
          Please implement this method to
          return the number of different combinations of US coins
          (penny: 1c, nickel: 5c, dime: 10c, quarter: 25c, half-dollar: 50c)
          which may be used to produce a given amount of money.

          For example, 11 cents can be produced with
          one 10-cent coin and one 1-cent coin,
          two 5-cent coins and one 1-cent coin,
          one 5-cent coin and six 1-cent coins,
          or eleven 1-cent coins.
          So there are four unique ways to produce 11 cents.
          Assume that the cents parameter is always positive.
         */
        return countMonet(cents, monets[0]);
    }

    private static int countMonet(int cents, int maxMonet) {
        int result = 0;
        for (int monet : monets) {
            if (monet <= maxMonet) {
                int diff = cents - monet;
                if (diff > 0) {
                    maxMonet = monet;
                    result += countMonet(diff, maxMonet);
                } else if (diff == 0) {
                    return 1;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(countWaysToProduceGivenAmountOfMoney(190));
    }
}
