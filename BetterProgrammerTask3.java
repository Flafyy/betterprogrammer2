package com.active;

import java.util.*;


public class BetterProgrammerTask3 {

    // Please do not change this interface
    public static interface Node {
        int getValue();

        List<Node> getChildren();
    }


    public static List<Node> traverseTreeInDepth(Node root) {
        /*
          Please implement this method to
          traverse the tree in depth and return a list of all passed nodes.

          The method shall work optimally with large trees.
         */
        if (root == null) {
            return new LinkedList<Node>();
        }

        List<Node> children = root.getChildren();
        if (children == null) {
            children = new LinkedList<Node>();
        }

        List<Node> result = new LinkedList<Node>(children);
        for (Node el : result) {
            result.addAll(traverseTreeInDepth(el));
        }
        return result;
    }
}
